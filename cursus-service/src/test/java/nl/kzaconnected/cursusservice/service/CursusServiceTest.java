package nl.kzaconnected.cursusservice.service;

import nl.kzaconnected.cursusservice.model.Dto.CursusDto;
import nl.kzaconnected.cursusservice.model.Dao.*;
import nl.kzaconnected.cursusservice.repository.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class CursusServiceTest {

    @TestConfiguration
    static class CursusServiceTestContextConfiguration {

        @Bean
        public CursusService cursusService() {
            return new CursusService();
        }
    }

    @Autowired
    private CursusService cursusService;
    @MockBean
    private CursusRepository cursusRepository;
    @MockBean
    private AttitudeRepository attitudeRepository;
    @MockBean
    private SlagingscriteriumRepository slagingscriteriumRepository;
    @MockBean
    private FunctieniveauRepository functieniveauRepository;
    @MockBean
    private StatusRepository statusRepository;

    private List<CursusDto> cursusDtos;
    private List<Cursus> cursussen;
    private Attitude attitude;
    private Slagingscriterium slagingscriterium;
    private Functieniveau functieniveau;
    private Status status;

    @Before
    public void init() throws Exception{
        attitude = Attitude.builder().id(1L).attitude("Attitude1").build();
        slagingscriterium = Slagingscriterium.builder().id(1L).slagingscriterium("Slagingscriterium1").build();
        functieniveau = Functieniveau.builder().id(1L).functieniveau("Functieniveau1").build();
        status = Status.builder().id(1L).status("Status1").build();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        List<Datum> cursusData = new ArrayList<>();
        cursusData.add(Datum.builder().datum(simpleDateFormat.parse("01-01-2018 08:00:00")).build());
        cursusData.add(Datum.builder().datum(simpleDateFormat.parse("01-01-2019 09:00:00")).build());

        List<Docent> cursusDocenten = new ArrayList<>();
        cursusDocenten.add(Docent.builder().naam("Docent1").build());
        cursusDocenten.add(Docent.builder().naam("Docent2").build());

        CursusDto cursusDto1 = CursusDto.builder()
                .id(1L)
                .naam("Cursus1")
                .attitude(attitude.getAttitude())
                .slagingscriterium(slagingscriterium.getSlagingscriterium())
                .functieniveau(functieniveau.getFunctieniveau())
                .status(status.getStatus())
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .maxdeelnemers(1)
                .beschrijving("Beschrijving1")
                .build();

        CursusDto cursusDto2 = CursusDto.builder()
                .id(2L)
                .naam("Cursus2")
                .attitude(attitude.getAttitude())
                .slagingscriterium(slagingscriterium.getSlagingscriterium())
                .functieniveau(functieniveau.getFunctieniveau())
                .status(status.getStatus())
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .maxdeelnemers(2)
                .beschrijving("Beschrijving2")
                .build();

        cursusDtos = new ArrayList<>();
        cursusDtos.add(cursusDto1);
        cursusDtos.add(cursusDto2);

        Cursus cursus1 = Cursus.builder()
                .id(1L)
                .naam("Cursus1")
                .attitude(attitude)
                .slagingscriterium(slagingscriterium)
                .functieniveau(functieniveau)
                .status(status)
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .maxDeelnemers(1)
                .beschrijving("Beschrijving1")
                .build();

        Cursus cursus2 = Cursus.builder()
                .id(2L)
                .naam("Cursus2")
                .attitude(attitude)
                .slagingscriterium(slagingscriterium)
                .functieniveau(functieniveau)
                .status(status)
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .maxDeelnemers(2)
                .beschrijving("Beschrijving2")
                .build();

        cursussen = new ArrayList<>();
        cursussen.add(cursus1);
        cursussen.add(cursus2);
    }

    @Test
    public void findOneShouldReturnCursusDto() {
        Mockito.when(cursusRepository.getOne(1L))
                .thenReturn(cursussen.get(0));
        CursusDto found = cursusService.findOne(1L);
        Assert.assertEquals(found,cursusDtos.get(0));
    }

    @Test
    public void findAllShouldReturnListOfCursusDtos() {
        Mockito.when(cursusRepository.findAll())
                .thenReturn(cursussen);
        List<CursusDto> found = cursusService.findAll();
        for (CursusDto cursusDto : found) {
            Assert.assertTrue(cursusDtos.stream().anyMatch(cursusDto::equals));
        }
    }

    @Test
    public void saveShouldReturnCursusDto() {
        Mockito.when(cursusRepository.save(cursussen.get(0)))
                .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
                .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
                .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
                .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
                .thenReturn(status);
        CursusDto found = cursusService.save(cursusDtos.get(0));
        Assert.assertEquals(found,cursusDtos.get(0));
    }

    @Test
    public void deleteShouldCallRepositoryMethodDelete(){
        cursusService.delete(1L);
        verify(cursusRepository).deleteById(1L);
    }
}