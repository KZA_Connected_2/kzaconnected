package nl.kzaconnected.cursusservice.repository;

import nl.kzaconnected.cursusservice.model.Dao.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends JpaRepository<Status,Long> {

    public Status findByStatus(String status);
}
