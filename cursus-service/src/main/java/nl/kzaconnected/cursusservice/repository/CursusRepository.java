package nl.kzaconnected.cursusservice.repository;

import nl.kzaconnected.cursusservice.model.Dao.Cursus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CursusRepository extends JpaRepository<Cursus,Long> {
}
